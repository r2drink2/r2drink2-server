Getting started:
    npm install

To run:
    export SERIAL_EMULATOR=simulator
    export SERIAL_PORT_NAME=/dev/ttyAMA0
    node web.js

Note: Options for SERIAL_EMULATOR are:
    "disabled" = talk to actual R2Drink2 Arduino
    "loopback" = talk through hardware serial port, but with RX looped back to TX
    "simulator" = do not use a hardware serial port, just emulate in pure software

