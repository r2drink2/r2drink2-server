var SERIAL_EMULATOR = "disabled";
//var SERIAL_EMULATOR = "loopback";
//var SERIAL_EMULATOR = "simulator";
if(process.env.SERIAL_EMULATOR) {
  SERIAL_EMULATOR = process.env.SERIAL_EMULATOR;
}
console.log("Emulator status", SERIAL_EMULATOR)


var _ = require('lodash-node');
var q = require('q');
var url = require('url');
var express = require('express');
var gzippo = require('gzippo');
var logger = require('morgan');
var bodyParser = require('body-parser');
var fs = require('fs');
var parse = require('csv-parse');
var moment = require('moment');
var jcopy = function(x){return JSON.parse(JSON.stringify(x));}
var wait = function(duration_ms){
  var deferred = q.defer();
  setTimeout(function(){
    deferred.resolve();
  }, duration_ms);
  return deferred.promise;
}


if(SERIAL_EMULATOR == "simulator") {
  var SerialPort = function() {
    this.on = function(evt, cbk) {
      if(evt == 'open') {
        setTimeout(cbk, 1000);
      }
      else if(evt == 'data') {
        console.log("setting new callback");
        this.data_cbk = cbk;
      }
    };
    this.write = function(data, cbk) {
      console.log("Writing", data); 
      cbk();
      this.data_cbk(data);
    }
    this.close = function(cbk) {
      setTimeout(cbk, 1000);
    }
  }
}
else {
  var SerialPort = require('serialport').SerialPort
}
var serial_port_name = '/dev/ttyAMA0';
if(process.env.SERIAL_PORT_NAME) {
  serial_port_name = process.env.SERIAL_PORT_NAME;
}



var app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server);

app.use(logger('dev'));


var CUP_SIZE_FLOZ = 6;
var CUP_EMPTY_WEIGHT_GRAMS = 8.8;
var ICE_DENSITY_GRAMS_PER_FLOZ = 27.62;

var POUR_POSITIONS = []
var INGREDIENTS = []
var RECIPES = []


fs.readFile("" + __dirname + "/drink_db.csv", function (err, data) {
  parse(data.toString(), {}, function(err, output){
    _.each(output, function(row){
      if(row.length > 1) {
        if(row[0] == 'ingredient_config') {
          for(var i=0; i<(row.length-3); i++) {
            var cell = row[i+3];
            if(cell){
              while(INGREDIENTS.length <= i) {
                INGREDIENTS.push({});
              }
              if(row[2] == 'valve_number') {
                cell = ("00"+cell.toString()).substr(-2);
              }
              if(row[2] == 'valve_position') {
                cell = ("00"+cell.toString()).substr(-2);
                if(POUR_POSITIONS.indexOf(cell) == -1) {
                  POUR_POSITIONS.push(cell);
                }
              }
              if(row[2].indexOf('_ms') > -1) {
                cell = parseInt(cell);
              }
              INGREDIENTS[i][row[2]] = cell;
            }
          }
        }
        if(row[0] == 'recipe') {
          var recipe_name = row[1];
          var serving_size_oz = parseFloat(row[2]);
          var ingredients = [];
          for(var i=0; i<(row.length-3); i++) {
            var cell = row[i+3];
            if(cell && INGREDIENTS[i]) {
              var ingredient = jcopy(INGREDIENTS[i]);
              ingredient['parts'] = 0;
              ingredient['dashes'] = 0;
              ingredient['splashes'] = 0;
              ingredient['tsps'] = 0;
              ingredient['garnish'] = 0;
              if(cell.indexOf('d') > -1) {
                ingredient['dashes'] = parseFloat(cell.replace('d',''));
              }
              else if(cell.indexOf('s') > -1) {
                ingredient['splashes'] = parseFloat(cell.replace('s',''));
              }
              else if(cell.indexOf('t') > -1) {
                ingredient['tsps'] = parseFloat(cell.replace('t',''));
              }
              else if(cell.indexOf('X') > -1) {
                ingredient['garnish'] = 1;
              }
              else {
                ingredient['parts'] = parseFloat(cell);
              }
              ingredients.push(ingredient);
            }
          }
          if(recipe_name && serving_size_oz) {
            var recipe = {
              'recipe_name': recipe_name,
              'serving_size_oz': serving_size_oz,
              'ingredients': ingredients
            }
            var image_url = "/assets/img/" + recipe_name.replace(/\W/g, '').toLowerCase() + ".jpg";
            var img_file = "" + __dirname + "/www" + image_url;
            recipe['target_image_url'] = image_url;
            if(fs.existsSync(img_file)) {
              recipe['image_url'] = image_url;
            }
            else {
              console.log("Missing image", image_url);
              recipe['image_url'] = '/assets/img/default_cocktail.jpg';
            }
            RECIPES.push(recipe);
          }
        }
      }
    });
    POUR_POSITIONS.sort();
    console.log("Loaded", POUR_POSITIONS.length, "pour positions");
    console.log("Loaded", INGREDIENTS.length, "ingredients");
    console.log("Loaded", RECIPES.length, "recipes");
  });
});


var MENU = [ {'menu_name': 'All', 'recipe_names': []} ]
fs.readFile("" + __dirname + "/drink_menu.csv", function (err, data) {
  parse(data.toString(), {}, function(err, output){
    _.each(output, function(row){
      if(row.length > 1) {
        if(row[0] == 'menu_name') {
          for(var i=2; i<(row.length); i++) {
            if(row[i]) {
              var new_menu = {'menu_name': row[i], 'recipe_names': []};
              MENU.push(new_menu);
            }
          }
        }
        else if(row[0] == 'recipe' && row[1]) {
          var recipe_name = row[1];
          for(var i=0; i<MENU.length; i++) {
            if(row[i+1]) {
              MENU[i].recipe_names.push(recipe_name);
            }
          }
        }
      }
    });
    console.log("Loaded", MENU.length, "menus");
  });
});




var SERIAL_PORT = null;
var SERIAL_RX_BUF = "";
var SERIAL_DISPATCH = {};
var write_serial = function(data, resp) {
  var deferred = q.defer();
  if(resp) {
    SERIAL_DISPATCH[resp] = function(){
      delete SERIAL_DISPATCH[resp];
      deferred.resolve();
    }
  }
  SERIAL_PORT.write(data, function(){
    console.log("write_serial", data);
    if(!resp) {
      deferred.resolve();
    }
  });
  return deferred.promise;
}


/*
SERIAL PORT PROTOCOL

RPI->Arduino 
  [bSTART] (start dispensing, retract cup arm, stop scale)
  [pPPvXX:YYYYvXX:YYYY] (move arm to PP, dispense liquids, XX = valve number in ASCII, YYYY = duration milliseconds)
  [eSTOP] (stop dispensing, extend cup arm, start scale)
  [tTARE] (begin scale calibration)
  
Arduino->RPI
  [gXXX.XXXX] (XXX.XXXX = current scale weight in grams)
  [aPP:CC] (pour status, PP = position number in ASCII, CC = status code)
  CC status codes: 
    BM = begin servo arm move
    EM = end servo arm move
    BP = begin batch pour
    EP = end batch pour 
  [vXX:YY] (valve status, XX = valve number in ASCII, YY = status code)
  YY status codes: 
    BP = begin valve pour
    EP = end valve pour   
  [c] (scale calibration complete)
  [sSS] (arm status, SS = status code)
  SS status codes:
    AR = cup arm retracted
    AE = cup arm extended
*/

SERIAL_LOOPBACK_EMULATOR = function(packet) {
  if(packet[1] == 'b') {
    console.log("serial emulator", "received arm retract", packet)
    SERIAL_LOOPBACK_SCALE_ENABLED = false;
    var resp = "[sAR]";
    wait(1000)
      .then(function(){
        return write_serial(resp);
      })
      .then(function(){
        console.log("serial emulator", "sent end arm retract", resp);
      });
  }
  if(packet[1] == 'e') {
    console.log("serial emulator", "received stop and arm extend", packet)
    SERIAL_LOOPBACK_SCALE_ENABLED = true;
    var resp = "[sAE]";
    wait(1000)
      .then(function(){
        return write_serial(resp);
      })
      .then(function(){
        console.log("serial emulator", "sent end stop and arm extend", resp);
      });
  }
  if(packet[1] == 't') {
    console.log("serial emulator", "received scale calibrate", packet)
    var resp = "[c]";
    wait(1000)
      .then(function(){
        return write_serial(resp);
      })
      .then(function(){
        console.log("serial emulator", "sent scale calibrated", resp);
      });
  }
  if(packet[1] == 'p') {
    var position = packet.substr(2,2);
    console.log("serial emulator", "received batch pour position", position, packet)
    write_serial("[a"+position+":BM]")
      .then(function(){
          console.log("serial emulator", "sent begin servo arm move position", position, "[a"+position+":BM]");
          return wait(2000);
      })
      .then(function(){
          return write_serial("[a"+position+":EM]");
      })
      .then(function(){
          console.log("serial emulator", "sent end servo arm move position", position, "[a"+position+":EM]");
          return wait(100);
      })
      .then(function(){
          return write_serial("[a"+position+":BP]");
      })
      .then(function(){
          console.log("serial emulator", "sent begin batch pour position", position, "[a"+position+":BP]");
          return wait(100);
      })
      .then(function(){
          var max_ms  = 1;
          pour_cmds = packet.split('v');
          for(var i=1; i<pour_cmds.length; i++){
            var duration_ms = parseFloat(pour_cmds[i].substr(3,4));
            max_ms = Math.max(max_ms, duration_ms)
          }
          console.log("WAITING", max_ms, "milliseconds");
          return wait(max_ms);
      })
      .then(function(){
          return wait(100);
      })
      .then(function(){
          return write_serial("[a"+position+":EP]");
      })
      .then(function(){
          console.log("serial emulator", "sent end batch pour position", position, "[a"+position+":EP]");
      });
  }
}

var CURRENT_RAW_SCALE_GRAMS = 0;
var CURRENT_ICE_FLOZ = 0;
var CURRENT_HAS_CUP = false;
SERIAL_DISPATCH["[g"] = function(weightPacket) {
  var weightGrams = parseFloat(weightPacket.substr(2, weightPacket.length-3));
  var hasCup = (weightGrams > 0.75*CUP_EMPTY_WEIGHT_GRAMS);
  var iceFlOz = Math.max((weightGrams - CUP_EMPTY_WEIGHT_GRAMS) / ICE_DENSITY_GRAMS_PER_FLOZ, 0);
  if((hasCup != CURRENT_HAS_CUP) || (Math.abs(iceFlOz - CURRENT_ICE_FLOZ) > 0.25)) {
    io.emit('scale', {'has_cup': hasCup, 'ice_fl_oz': iceFlOz, 'weight_grams': weightGrams});
  }
  CURRENT_ICE_FLOZ = iceFlOz;
  CURRENT_HAS_CUP = hasCup;
  CURRENT_RAW_SCALE_GRAMS = weightGrams;
}


var POUR_STATE = {}

var pour_position = function(valve_position, valve_durations) {
  console.log("Started pour_position", valve_position, valve_durations);
  var deferred = q.defer();
  valve_position = ("00"+valve_position.toString()).substr(-2);
  cmd = "[p" + valve_position;
  _.forOwn(valve_durations, function(duration_ms, valve_num) {
    valve_num = ("00"+valve_num.toString()).substr(-2);
    valve_duration = ("0000"+Math.round(duration_ms).toString()).substr(-4);
    cmd = cmd + 'v' + valve_num + ":" + valve_duration;
  });
  cmd = cmd + "]";
  resp = "[a"+valve_position+":EP]";
  write_serial(cmd, resp).then(function(){
    console.log("Ended pour_position", valve_position, valve_durations);
    deferred.resolve();
  });
  return deferred.promise;
}

var begin_pour = function(){
  console.log("Started begin_pour");
  var deferred = q.defer();
  write_serial("[bSTART]", "[sAR]").then(function(){
    console.log("Finished begin_pour");
    deferred.resolve();
  });
  return deferred.promise;
}

var end_pour = function(){
  console.log("Started end_pour");
  var deferred = q.defer();
  write_serial("[eSTOP]", "[sAE]").then(function(){
    POUR_STATE['status'] = 'idle';
    POUR_STATE['name'] = '';
    POUR_STATE['ingredients'] = [];
    io.emit('pour', POUR_STATE);
    console.log("Finished end_pour");
    deferred.resolve();
  });
  return deferred.promise;
}

var calibrate_scale = function(){
  console.log("Started calibrate_scale");
  var deferred = q.defer();
  write_serial("[tTARE]", "[c]").then(function(){
    console.log("Finished calibrate_scale");
    deferred.resolve();
  });
  return deferred.promise;
}



var do_pour = function(recipe) {
  POUR_STATE['status'] = 'pouring';
  POUR_STATE['name'] = recipe['recipe_name'];
  POUR_STATE['ingredients'] = recipe['ingredients'];
  io.emit('pour', POUR_STATE);

  var total_parts = _.sum(_.pluck(recipe['ingredients'], 'parts'));
  var pour_oz = Math.min(CURRENT_ICE_FLOZ + recipe['serving_size_oz'], CUP_SIZE_FLOZ) - CURRENT_ICE_FLOZ;
  var pour_oz_per_part = total_parts == 0 ? 0 : (pour_oz / total_parts);

  var pour_commands = [];
  pour_commands.push(function() {
    return begin_pour();
  });

  var ingredients = JSON.parse(JSON.stringify(recipe['ingredients']));
  _.each(ingredients, function(ingredient){
    ingredient['pour_duration'] = ingredient['parts'] * pour_oz_per_part * ingredient['pour_speed_ms_per_oz'] +
                                                 ingredient['dashes'] * ingredient['pour_speed_ms_per_dash'] +
                                                 ingredient['splashes'] * ingredient['pour_speed_ms_per_dash'] +
                                                 ingredient['tsps'] * ingredient['pour_speed_ms_per_tsp'];
  });
  _.each(POUR_POSITIONS, function(pour_pos) {
    var done = false;
    while(!done) {
      (function(){
        var pour_arg = {};
        var pour_delay_ms = 0;
        _.each(ingredients, function(ingredient){
          if((ingredient['valve_position'] == pour_pos) && (ingredient['pour_duration'] > 0)) {
            var pour_duration = Math.min(ingredient['pour_duration'], ingredient['max_pour_duration_ms']);
            pour_arg[ingredient['valve_number']] = pour_duration;
            ingredient['pour_duration'] = ingredient['pour_duration'] - pour_duration;
            pour_delay_ms = Math.max(pour_delay_ms, ingredient['post_pour_delay_ms']);
          }
        });
        if(_.keys(pour_arg).length > 0) {
          pour_commands.push(function() {
            console.log("Requesting pour", pour_pos, pour_arg);
            return pour_position(pour_pos, pour_arg);
          });
          pour_commands.push(function() {
            console.log("Waiting to clear pour", pour_pos, pour_delay_ms);
            return wait(pour_delay_ms);
          });
        }
        else {
          done = true;
        }
      })();
    }
  })
  pour_commands.push(function() {
    return end_pour();
  });
  return pour_commands.reduce(q.when, q());
}

console.log("Starting server");
server.listen(process.env.PORT || 5000, function(){
  console.log("Server started");
  console.log("Opening serial port", serial_port_name);
  SERIAL_PORT = new SerialPort(serial_port_name, {
    baudrate: 9600
  });
  SERIAL_PORT.on('open', function(){
    console.log("Serial port opened", serial_port_name);
    end_pour().then(function(){
      console.log("Startup complete");
    });
  });
  SERIAL_PORT.on('data', function(data) {
      SERIAL_RX_BUF = SERIAL_RX_BUF + data;
      var packets = SERIAL_RX_BUF.match(/\[[^\[\]]+\]/g);
      SERIAL_RX_BUF = SERIAL_RX_BUF.replace(/\[[^\[\]]+\]/g, '');
      last_open = SERIAL_RX_BUF.lastIndexOf("[");
      if(last_open == -1) {
        SERIAL_RX_BUF = "";
      }
      else {
        SERIAL_RX_BUF = SERIAL_RX_BUF.substr(last_open);
      }
      if(packets) {
        for(var i=0; i<packets.length; i++) {
          //console.log("Received packet", packets[i])
          _.each(_.keys(SERIAL_DISPATCH), function(dispatch_key) {
            if(packets[i].indexOf(dispatch_key) == 0) {
              SERIAL_DISPATCH[dispatch_key](packets[i]);
            }
          });
          if(SERIAL_EMULATOR != "disabled") {
            SERIAL_LOOPBACK_EMULATOR(packets[i]);
          }
        }
      }
    });
});

var shutdown = function(){
  console.log("Shutting down server");
  end_pour()
    .then(function(){
      return begin_pour();
    })
    .then(function(){
      console.log("Closing serial port", serial_port_name);
      SERIAL_PORT.close(function(){
        console.log("Serial port closed", serial_port_name);
        console.log("Shutdown complete");
        process.exit();
      });
    });
}
// TERM signal .e.g. kill 
process.on('SIGTERM', shutdown);
// INT signal e.g. Ctrl-C
process.on('SIGINT', shutdown);  



app.use('/api', bodyParser.json())


// $.postJSON('/api/scale/tare', {})
app.post('/api/scale/tare', function(req, res){
  calibrate_scale();
  res.json({});
});


app.post('/api/bleed/start', function(req, res){
  begin_pour();
  res.json({});
});

app.post('/api/bleed/pour', function(req, res){
  pour_position(req.body.valve_position, req.body.valve_durations);
  res.json({});
});


LOGFILE = "" + __dirname + "/r2drink2.log.json"

// $.get('/api/recipes').success(function(data){$.postJSON('/api/pour', data[0])})
app.post('/api/pour', function(req, res){
  req.body.timestamp = moment.utc().format();
  var logline = JSON.stringify(req.body) + ",\n";
  fs.appendFile(LOGFILE, logline, function (err) {
  });
  do_pour(req.body);
  res.json(POUR_STATE);
});

// $.get('/api/pour').success(function(data){console.log(data)})
app.get('/api/pour', function(req, res){
  res.json(POUR_STATE);
});

STATUS_MESSAGE = { message_text: "System Initialized" }

// $.get('/api/status_message').success(function(data){console.log(data)})
app.get('/api/status_message', function(req, res){
  res.json(STATUS_MESSAGE);
});

// $.postJSON('/api/status_message', {'message_text': 'Hello world'}).success(function(data){console.log(data)})
app.post('/api/status_message', function(req, res){
  STATUS_MESSAGE = req.body;
  io.emit('status_message', STATUS_MESSAGE);
  res.json(STATUS_MESSAGE);
});

// $.get('/api/scale').success(function(data){console.log(data)})
app.get('/api/scale', function(req, res){
  res.json({'has_cup': CURRENT_HAS_CUP, 'ice_fl_oz': CURRENT_ICE_FLOZ, 'weight_grams': CURRENT_RAW_SCALE_GRAMS});
});

// $.postJSON('/api/scale', {'has_cup': false, 'ice_fl_oz': 0, 'weight_grams': 1.0}).success(function(data){console.log(data)})
// $.postJSON('/api/scale', {'has_cup': true, 'ice_fl_oz': 0, 'weight_grams': 9.0}).success(function(data){console.log(data)})
// $.postJSON('/api/scale', {'has_cup': true, 'ice_fl_oz': 4, 'weight_grams': 20.0}).success(function(data){console.log(data)})
app.post('/api/scale', function(req, res){
  CURRENT_HAS_CUP = req.body['has_cup'];
  CURRENT_HAS_CUP = req.body['has_cup'];
  CURRENT_RAW_SCALE_GRAMS = req.body['weight_grams'];
  io.emit('scale', {'has_cup': CURRENT_HAS_CUP, 'ice_fl_oz': CURRENT_ICE_FLOZ, 'weight_grams': CURRENT_RAW_SCALE_GRAMS});
  res.json({'has_cup': CURRENT_HAS_CUP, 'ice_fl_oz': CURRENT_ICE_FLOZ, 'weight_grams': CURRENT_RAW_SCALE_GRAMS});
});


// $.delete('/api/pour')
app.delete('/api/pour', function(req, res){
  end_pour();
  res.json(POUR_STATE);
});

// $.get('/api/ingredients').success(function(data){console.log(data)})
app.get('/api/ingredients', function(req, res){
  res.json(INGREDIENTS);
});

// $.get('/api/recipes').success(function(data){console.log(data)})
app.get('/api/recipes', function(req, res){
  res.json(RECIPES);
});

// $.get('/api/menu').success(function(data){console.log(data)})
app.get('/api/menu', function(req, res){
  res.json(MENU);
});


// $.get('/api/recipes/template').success(function(data){console.log(data)})
app.get('/api/recipes/template', function(req, res){
  var ingredients = jcopy(INGREDIENTS);
  _.each(ingredients, function(ingredient){
    ingredient['parts'] = 0;
  });
  res.json({
            'recipe_name': 'Custom',
            'serving_size_oz': 5,
            'ingredients': ingredients
          });
});


app.use(gzippo.staticGzip("" + __dirname + "/www"));


io.on('connection', function (socket) {
  socket.emit('pour', POUR_STATE);
});


