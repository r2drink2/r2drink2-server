angular.module('drinks')
    .directive('fillCup', function () {
    return {
        restrict: 'EA', //E = element, A = attribute, C = class, M = comment         
        scope: {
            //@ reads the attribute value, = provides two-way binding, & works with functions
            servingSizeOz: '=',
            iceSizeOz: '=',
            cupSizeOz: '=',
        },
        templateUrl: '/src/drinks/view/fillCup.html',
        link: function ($scope, element, attrs) {

        	var recalc = function() {

	         	var cupSizeOz
	         	var canvas = $(element).find("canvas")[0];
	         	var width = $("canvas")[0].width;
	         	var height = $("canvas")[0].height;

	         	var height_per_oz = height / $scope.cupSizeOz;
	         	var common_oz = Math.min($scope.servingSizeOz, $scope.iceSizeOz);

	         	var common_height = Math.max(common_oz * height_per_oz, 0);
	         	var liquid_height = Math.max(($scope.servingSizeOz - common_oz)*height_per_oz, 0);
	         	var ice_height = Math.max(($scope.iceSizeOz - common_oz)*height_per_oz, 0);

	         	var liquid_start = 0;
	         	var liquid_pic_height = Math.min(liquid_height, height);
	         	var common_start = liquid_start + liquid_pic_height;
	         	var common_pic_height = Math.min(common_start+common_height, height)-common_start;
	         	var ice_start = common_start + common_pic_height;
	         	var ice_pic_height = Math.min(ice_start+ice_height, height)-ice_start;
	         	var empty_start = ice_start + ice_pic_height;
	         	var empty_pic_height = Math.min(empty_start+height, height)-empty_start;

	         	var empty_start_y = 0;
	         	var ice_start_y = empty_start_y + empty_pic_height;
	         	var common_start_y = ice_start_y + ice_pic_height;
	         	var liquid_start_y = common_start_y + common_pic_height;

	         	var ctx = canvas.getContext("2d");

				var empty_cup = new Image();
				var empty_cup_ice = new Image();
				var full_cup_ice = new Image();
				var full_cup = new Image();
				empty_cup.onload = function () {
				    ctx.drawImage(empty_cup,
				    				0,	empty_start_y/height*empty_cup.height,	empty_cup.width,	empty_pic_height/height*empty_cup.height,
				    				0,	empty_start_y,	width,	empty_pic_height);
				}
				empty_cup_ice.onload = function () {
				    ctx.drawImage(empty_cup_ice,
				    				0,	ice_start_y/height*empty_cup_ice.height,	empty_cup_ice.width,ice_pic_height/height*empty_cup_ice.height,
				    				0,	ice_start_y,	width,	ice_pic_height);
				}
				full_cup_ice.onload = function () {
				    ctx.drawImage(full_cup_ice,
				    				0,	common_start_y/height*full_cup_ice.height,	full_cup_ice.width,	common_pic_height/height*full_cup_ice.height,
				    				0,	common_start_y,	width,	common_pic_height);
				}
				full_cup.onload = function () {
				    ctx.drawImage(full_cup,
				    				0,	liquid_start_y/height*full_cup.height,	full_cup.width,	liquid_pic_height/height*full_cup.height,
				    				0,	liquid_start_y,	width,	liquid_pic_height);
				}
				empty_cup.src = "/assets/img/EmptyCup.png";
				empty_cup_ice.src = "/assets/img/EmptyCupIce.png";
				full_cup_ice.src = "/assets/img/FullCupIce.png";
				full_cup.src = "/assets/img/FullCup.png";

        	}

         	recalc();

			$scope.$watch('servingSizeOz', function(newValue, oldValue) {
                if (newValue != oldValue) {
                	recalc();
                }
            });
			$scope.$watch('iceSizeOz', function(newValue, oldValue) {
                if (newValue != oldValue) {
                	recalc();
                }
            });

        }
    }
});