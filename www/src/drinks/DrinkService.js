(function(){
  'use strict';

  angular.module('drinks')
         .service('drinkService', [
            '$q', 
            '$rootScope',
            'ALL_RECIPES',
            'MENU',
            function ($q, $rootScope, ALL_RECIPES, MENU){

              var socket = io(window.location.origin);
              socket.on('connect', function() {
                  console.log('connected');
              });
              socket.on('pour', function (data) {
                $rootScope.$emit('pour', data);
              });
              socket.on('scale', function (data) {
                $rootScope.$emit('scale', data);
              });
              socket.on('status_message', function (data) {
                $rootScope.$emit('status_message', data);
              });


              var doSearch = function(menu_name) {
                var recipe_names = _.find(MENU, function(menu_item) {
                  return menu_item.menu_name == menu_name;
                }).recipe_names;
                return _.filter(ALL_RECIPES, function(recipe) {
                  return (recipe_names.indexOf(recipe.recipe_name) > -1);
                });
              };


              return {
                doSearch: doSearch,
                drinkCategories: function(){
                  return _.map(MENU, function(menu_item) {
                    return {'category': menu_item.menu_name,
                           'display_name': menu_item.menu_name}
                  });
                },
              };
            }
            ]);
})();
