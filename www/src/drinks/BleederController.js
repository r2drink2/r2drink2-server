(function(){

  angular
       .module('drinks')
       .controller('BleederController', [
          'ALL_INGREDIENTS',
          'drinkService', 
          '$scope',
          '$rootScope',
          '$window', 
          '$http',
          '$log', 
          '$q',
          function ( ALL_INGREDIENTS, drinkService, $scope, $rootScope, $window, $http, $log, $q) {
            var self = this;

            self.ALL_INGREDIENTS = JSON.parse(JSON.stringify(ALL_INGREDIENTS));
            self.pour_state = {};
            self.scale_state = {};
            self.status_message = {};

            self.bleed_time_ms = {};
            _.each(ALL_INGREDIENTS, function(ingredient) {
              self.bleed_time_ms[ingredient.ingredient_name] = 0;
            });

            $http.get('/api/status_message').then(function(statusMessage){
              self.status_message = statusMessage.data;
            });

            self.bleed = function(ingredient, duration_ms) {
              var bleed_req = {'valve_position': ingredient.valve_position,
                               'valve_durations': {} };
              console.log("Starting bleed", bleed_req);
              bleed_req['valve_durations'][ingredient.valve_number] = duration_ms;
              $.postJSON('/api/bleed/pour', bleed_req).then(function() {
                console.log("Bleed started");
              });

            }

            self.start_pour = function() {
              console.log("Starting pour");
              $.postJSON('/api/bleed/start', {}).then(function() {
                console.log("Pour started");
              });
            }

            self.end_pour = function() {
              console.log("Ending pour");
              $.delete('/api/pour').then(function() {
                console.log("Pour ended");
              });
            }

            self.tare = function() {
              console.log("Starting tare scale");
              $.postJSON('/api/scale/tare', {}).then(function() {
                console.log("Scale tared");
              });
            }
            
            self.set_status_message = function() {
              console.log("Setting status message");
              $.postJSON('/api/status_message', self.status_message).then(function() {
                console.log("Status message set");
              });
            }
            

            var cleanUpPourState = $rootScope.$on('pour', function(event, msg) {
              $scope.$apply(function(){
                self.pour_state = msg;
              });
            });
            var cleanUpScaleState = $rootScope.$on('scale', function(event, msg) {
              $scope.$apply(function(){
                self.scale_state = msg;
                console.log("wut", self.scale_state);
              });
            });

            $scope.$on('$destroy', function() {
              cleanUpPourState();
              cleanUpScaleState();
            });
          }
       ]);
})();
