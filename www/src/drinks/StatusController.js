(function(){

  angular
       .module('drinks')
       .controller('StatusController', [
          'drinkService', 
          '$scope',
          '$rootScope',
          '$window', 
          '$http',
          '$log', 
          '$q',
          function ( drinkService, $scope, $rootScope, $window, $http, $log, $q) {
            var self = this;

            self.pour_state = {};
            self.scale_state = {};
            self.status_message = {};

            self.reload = function(){
              $window.location.reload();
            }

            $http.get('/api/pour').then(function(pourState){
              if(pourState.data['status'] != 'idle') {
                self.pour_state = pourState.data;
              }
            });
            $http.get('/api/scale').then(function(scaleState){
              self.scale_state = scaleState.data;
            });
            $http.get('/api/status_message').then(function(statusMessage){
              self.status_message = statusMessage.data;
            });

            var cleanUpPourState = $rootScope.$on('pour', function(event, msg) {
              $scope.$apply(function(){
                self.pour_state = msg;
              });
            });
            var cleanUpScaleState = $rootScope.$on('scale', function(event, msg) {
              $scope.$apply(function(){
                self.scale_state = msg;
              });
            });
            var cleanUpMessageState = $rootScope.$on('status_message', function(event, msg) {
              $scope.$apply(function(){
                self.status_message = msg;
              });
            });
            $scope.$on('$destroy', function() {
              cleanUpPourState();
              cleanUpScaleState();
              cleanUpMessageState();
            });
          }
       ]);
})();
