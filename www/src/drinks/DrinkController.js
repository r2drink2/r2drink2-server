(function(){

  angular
       .module('drinks')
       .controller('DrinkController', [
          'ALL_INGREDIENTS', 
          'drinkService', 
          '$scope',
          '$rootScope',
          '$mdDialog', 
          '$http',
          '$log', 
          '$q',
          function (ALL_INGREDIENTS, drinkService, $scope, $rootScope, $mdDialog, $http, $log, $q) {
            var self = this;

            self.showCarousel = false;
            self.currentDrinks = [];
            self.currentDrinkIndex = 0;
            self.drinkCategories = drinkService.drinkCategories();
            self.currentCategory = self.drinkCategories[0];
            self.currentDrink = {};
            self.currentIceFlOz = 0;
            self.currentHasCup = false;


            self.busyPour = false;
            var showPourDialog = function(drink, initialState) {
              self.busyPour = true;

              $mdDialog.hide();
              $mdDialog.show({
                templateUrl: '/src/drinks/view/pourDialog.html',
                locals: {
                  currentDrink: drink,
                  initialState: initialState,
                },
                controller: function($scope, $mdDialog, $http, $interval, currentDrink, initialState) {
                  $scope.currentState = initialState;
                  $scope.currentDrink = JSON.parse(JSON.stringify(currentDrink));
                  $scope.ALL_INGREDIENTS = JSON.parse(JSON.stringify(ALL_INGREDIENTS));

                  $scope.CUP_CONFIRM_TIME = 3000;
                  $scope.THANK_YOU_TIME = 3000;

                  $scope.beginTime = new Date();
                  $scope.remainingTime = 0;
                  $scope.busy = false;
                  $scope.Math = Math;
                  $scope.ingredientFilter = function(ingredientType) {
                    return function(ingredient) {
                      return ingredient[ingredientType] && ingredient[ingredientType] > 0;
                    }
                  }

                  var prepIngredients = function() {
                    _.each($scope.currentDrink.ingredients, function(ingredient) {
                      if(ingredient.ingredient_type == 'splasher') {
                        ingredient.chosen_type = 'dashes';
                        ingredient.chosen_amount = 0;
                      }
                      else {
                        ingredient.chosen_type = 'parts';
                        ingredient.chosen_amount = 0;
                      }
                      if(ingredient.parts > 0) {
                        ingredient.chosen_type = 'parts';
                        ingredient.chosen_amount = ingredient.parts;
                      }
                      else if(ingredient.tsps > 0) {
                        ingredient.chosen_type = 'tsps';
                        ingredient.chosen_amount = ingredient.tsps;
                      }
                      else if(ingredient.splashes > 0) {
                        ingredient.chosen_type = 'splashes';
                        ingredient.chosen_amount = ingredient.splashes;
                      }
                      else if(ingredient.dashes > 0) {
                        ingredient.chosen_type = 'dashes';
                        ingredient.chosen_amount = ingredient.dashes;
                      }
                      else if(ingredient.garnish > 0) {
                        ingredient.chosen_type = 'garnish';
                        ingredient.chosen_amount = ingredient.garnish;
                      }
                    });
                  }
                  prepIngredients();

                  var cleanIngredients = function() {
                    _.remove($scope.currentDrink.ingredients, function(ingredient) {
                      if(ingredient.chosen_amount == 0) {
                        console.log("removing", ingredient.ingredient_name);
                        return true;
                      }
                      return false;
                    });
                    _.each($scope.currentDrink.ingredients, function(ingredient) {
                      if(ingredient.chosen_type == 'parts') {
                        ingredient.parts = ingredient.chosen_amount;
                        ingredient.tsps = 0;
                        ingredient.splashes = 0;
                        ingredient.dashes = 0;
                        ingredient.garnish = 0;
                        console.log(ingredient.parts, 'parts', ingredient.ingredient_name);
                      }
                      else if(ingredient.chosen_type == 'tsps') {
                        ingredient.parts = 0;
                        ingredient.tsps = ingredient.chosen_amount;
                        ingredient.splashes = 0;
                        ingredient.dashes = 0;
                        ingredient.garnish = 0;
                        console.log(ingredient.tsps, 'tsps', ingredient.ingredient_name);
                      }
                      else if(ingredient.chosen_type == 'splashes') {
                        ingredient.parts = 0;
                        ingredient.tsps = 0;
                        ingredient.splashes = ingredient.chosen_amount;
                        ingredient.dashes = 0;
                        ingredient.garnish = 0;
                        console.log(ingredient.splashes, 'splashes', ingredient.ingredient_name);
                      }
                      else if(ingredient.chosen_type == 'dashes') {
                        ingredient.parts = 0;
                        ingredient.tsps = 0;
                        ingredient.splashes = 0;
                        ingredient.dashes = ingredient.chosen_amount;
                        ingredient.garnish = 0;
                        console.log(ingredient.dashes, 'dashes', ingredient.ingredient_name);
                      }
                      else if(ingredient.chosen_type == 'garnish') {
                        ingredient.parts = 0;
                        ingredient.tsps = 0;
                        ingredient.splashes = 0;
                        ingredient.dashes = 0;
                        ingredient.garnish = ingredient.chosen_amount;
                        console.log(ingredient.garnish, 'garnish', ingredient.ingredient_name);
                      }
                    });
                  }


                  $scope.realIngredientFilter = function(ingredient) {
                      return ingredient.chosen_type != 'garnish';
                    }

                  var goWaitForGlass = function() {
                    cleanIngredients();
                    console.log($scope.currentDrink);

                    $scope.busy = true;
                    $scope.currentState = 'wait_for_glass';
                    $scope.beginTime = new Date();
                    $scope.busy = false;
                  }
                  var goConfirmGlass = function() {
                    $scope.busy = true;
                    $scope.currentState = 'confirm_glass';
                    $scope.beginTime = new Date();
                    $scope.busy = false;
                    $scope.remainingTime = Math.max($scope.CUP_CONFIRM_TIME - ((new Date() - $scope.beginTime)), 0);
                  }
                  var goWaitForPour = function() {
                    $scope.busy = true;
                    $scope.beginTime = new Date();
                    $.postJSON('/api/pour', $scope.currentDrink).then(function() {
                      $scope.currentState = 'wait_for_pour';
                      $scope.busy = false;
                    });
                  }
                  var goWaitForTake = function() {
                    $scope.busy = true;
                    $scope.currentState = 'wait_for_take';
                    $scope.beginTime = new Date();
                    $scope.busy = false;
                  }
                  var goDrinkTaken = function() {
                    $scope.busy = true;
                    $scope.currentState = 'drink_taken';
                    $scope.beginTime = new Date();
                    $scope.busy = false;
                    $scope.remainingTime = Math.max($scope.THANK_YOU_TIME - ((new Date() - $scope.beginTime)), 0);
                  }
                  var goClose = function() {
                    $scope.busy = true;
                    $scope.currentState = 'finished';
                    $mdDialog.hide();
                    self.showDrinkList();
                  }

                  var stateInterval = $interval(function() {
                    if($scope.busy) {
                      return;
                    }
                    if($scope.currentState == 'wait_for_glass') {
                      $scope.busy = true;
                      $http.get('/api/scale').then(function(scaleState){
                        if(scaleState.data['has_cup']) {
                          goConfirmGlass();
                        }
                        else {
                          $scope.busy = false;
                        }
                      });
                    }
                    else if($scope.currentState == 'confirm_glass') {
                      $scope.busy = true;
                      $scope.remainingTime = Math.max($scope.CUP_CONFIRM_TIME - ((new Date() - $scope.beginTime)), 0);
                      $http.get('/api/scale').then(function(scaleState){
                        if(!scaleState.data['has_cup']) {
                          goWaitForGlass();
                        }
                        else if((new Date() - $scope.beginTime) > $scope.CUP_CONFIRM_TIME) {
                          goWaitForPour();
                        }
                        else {
                          $scope.busy = false;
                        }
                      });
                    }
                    else if($scope.currentState == 'wait_for_pour') {
                      $scope.busy = true;
                      $http.get('/api/pour').then(function(pourState){
                        if(pourState.data['status'] == 'idle') {
                          goWaitForTake();
                        }
                        else {
                          $scope.busy = false;
                        }
                      });
                    }
                    else if($scope.currentState == 'wait_for_take') {
                      $scope.busy = true;
                      $http.get('/api/scale').then(function(scaleState){
                        if(!scaleState.data['has_cup']) {
                          goDrinkTaken();
                        }
                        else {
                          $scope.busy = false;
                        }
                      });
                    }
                    else if($scope.currentState == 'drink_taken') {
                      $scope.busy = true;
                      $scope.remainingTime = Math.max($scope.THANK_YOU_TIME - ((new Date() - $scope.beginTime)), 0);
                      if((new Date() - $scope.beginTime) > $scope.THANK_YOU_TIME) {
                        goClose();
                      }
                      else {
                        $scope.busy = false;
                      }
                    }

                  }, 250);

                  $scope.skipCupCheck = function() {
                    goWaitForPour();
                  }

                  $scope.gimme = function() {
                    goWaitForGlass();
                  }

                  $scope.done = function() {
                    goClose();
                  }

                  $scope.cancel = function() {
                    $.delete('/api/pour').then(function() {
                      goClose();
                    });
                  }

                  $scope.$on('$destroy', function() {
                    $interval.cancel(stateInterval);
                    self.busyPour = false;
                  });

                },
              });

            };

            var updateCurrentDrink = function(){
              self.currentDrink = JSON.parse(JSON.stringify(self.currentDrinks[self.currentDrinkIndex]));
              console.log("setting current drink to", self.currentDrink);
            }

            self.searchDrinks = function(drink_category) {
              self.currentCategory = drink_category;
              self.currentDrinks = drinkService.doSearch(drink_category.category);;
              self.currentDrinkIndex = 0;
              updateCurrentDrink();
              self.showCarousel = false;
            }
            self.searchDrinks(self.currentCategory);

            self.selectDrink = function(drink_name) {
              self.currentDrinkIndex = _.findIndex(self.currentDrinks, function(drink){
                return drink.recipe_name == drink_name;
              });
              updateCurrentDrink();
              self.showCarousel = true;
            }

            $scope.$watch('dl.currentDrinkIndex', function(newValue, oldValue) {
              updateCurrentDrink();
            });

            self.showDrinkList = function(){
              self.showCarousel = false;
            }

            self.gimme = function(){
              showPourDialog(self.currentDrink, 'wait_for_glass');
            }

            self.customize = function(){
              showPourDialog(self.currentDrink, 'customize');
            }

            self.ingredientFilter = function(ingredientType) {
              return function(ingredient) {
                return ingredient[ingredientType] && ingredient[ingredientType] > 0;
              }
            }


            $http.get('/api/pour').then(function(pourState){
              if(pourState.data['status'] != 'idle') {
                showPourDialog({}, 'wait_for_pour');
              }
            });
            $http.get('/api/scale').then(function(scaleState){
              self.currentIceFlOz = scaleState.data['ice_fl_oz'];
              self.currentHasCup = scaleState.data['has_cup'];
            });

            var cleanUpPourState = $rootScope.$on('pour', function(event, msg) {
              if(!self.busyPour && (msg['status'] != 'idle')) {
                showPourDialog({}, 'wait_for_pour');
              }
            });
            var cleanUpScaleState = $rootScope.$on('scale', function(event, msg) {
              $scope.$apply(function(){
                self.currentIceFlOz = msg['ice_fl_oz'];
                self.currentHasCup = msg['has_cup'];
              });
            });
            $scope.$on('$destroy', function() {
              cleanUpPourState();
              cleanUpScaleState();
            });

          }
       ]);
})();
